# -*- coding: utf-8 -*-

from Robot import *
from Wall import *
from Target import *
from Arena import *
import pygame
import random
pygame.init()

size = (25,25)
white = (255, 255, 255)
blue = (0, 0, 255)
red = (255, 0, 0)
green = (0, 255, 50)
black = (0, 0, 0)

wall1 = (100, 430, 250, 10)
wall2 = (100, 100, 10, 330)
wall3 = (110, 100, 580, 10)
wall4 = (690, 100, 10, 330)
wall5 = (450, 430, 250, 10)
list_walls = [wall1, wall2, wall3, wall4, wall5]


def main():
    screen = pygame.display.set_mode((800, 640))
    pygame.display.set_caption("Combat Biplane")
    pygame.key.set_repeat(1, 1)

    background = pygame.Surface(screen.get_size())
    background.fill(black)

    arena = Arena(green, (110,110,580,320))
    arenagroup = pygame.sprite.Group(arena)

    robot = Robot(blue,400,500,size, screen)
    robotgroup = pygame.sprite.Group(robot)
    walls = pygame.sprite.Group()
    for wall in list_walls:
        walls.add(Wall(white,wall))
    targets = pygame.sprite.Group()
    for x in range(0,5):
        x = random.randrange(111,570)
        y = random.randrange(111,310)
        targets.add(Target(red,x,y,(8,8),screen))  

    clock = pygame.time.Clock()
    keepGoing = True
    while keepGoing:
        # taxa do frame
        clock.tick(100)
        #robots
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                keepGoing = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    robot.turnLeft()
                elif event.key == pygame.K_RIGHT:
                    robot.turnRight()
                elif event.key == pygame.K_UP:
                    robot.turnUp()
                elif event.key == pygame.K_DOWN:
                    robot.turnDown()
                elif event.key == pygame.K_SPACE:
                    robot.dropTarget(arenagroup)

        arenagroup.clear(screen, background)
        robotgroup.clear(screen, background)
        walls.clear(screen, background)
        targets.clear(screen, background)
        robotgroup.update(walls, targets)
        targets.update()

        #desenha no screen
        arenagroup.draw(screen)
        walls.draw(screen)
        robotgroup.draw(screen)
        targets.draw(screen)
        pygame.display.flip()

main()

