# -*- coding: utf-8 -*-

import pygame
import math

class Robot(pygame.sprite.Sprite):
    def __init__(self, cor, x, y, size, screen):
        pygame.sprite.Sprite.__init__(self)           
        self.imageMaster = pygame.Surface(size)
        self.imageMaster.fill(cor)
        self.imageMaster.set_colorkey((255, 0, 0))
        pygame.draw.polygon(self.imageMaster, (230, 130, 30), [(12.5,12.5),(0,0),(0,25)])
        self.imageMaster = self.imageMaster.convert()
        self.image = self.imageMaster
        #get the rectangular area of the Surface
        self.rect = self.image.get_rect()
        self.rect.center = (x, y)
        self.x = self.rect.centerx
        self.y = self.rect.centery
        #angulo
        self.degree = 90
        #x/y temporario
        self.dx = 100
        self.dy = 100
        #velocidade de movimentacao
        self.speed = 0
        self.screen = screen
        self.target = None

    def update(self, walls, targets):
        """Atualiza a posicao da imagem no screen
        """
        oldCenter = self.rect.center
        #guarda a posicao antiga, para o caso de a nova gerar colisao
        oldImage = self.image
        oldRect = self.rect
        oldX = self.x
        oldY = self.y
        size = self.imageMaster
        self.image = pygame.transform.rotate(self.imageMaster, self.degree)
        self.rect = self.image.get_rect()
        self.rect.center = oldCenter

        self.calcPos()
        self.x += self.dx
        self.y += self.dy
        self.rect.centerx = self.x
        self.rect.centery = self.y

        #nao deixar o robo sair dos limites da tela
        self.image_w, self.image_h = self.image.get_size()
        bounds_rect = self.screen.get_rect().inflate(
                        -self.image_w, -self.image_h)
        
        #se o robo nao esta levando um target ainda
        if not self.target:
            #verifica se o robo colide com algum target
            collidetargets = pygame.sprite.spritecollide(self, targets, False)
            if collidetargets:
                #caso colida com algum target, atribui esse target ao robo, para os dois andarem juntos
                self.target = collidetargets[0]

        #from creeps
        #testa colisao com os limites da tela e com as paredes da arena
        collide = self.x < bounds_rect.left or self.x > bounds_rect.right or self.y < bounds_rect.top or self.y > bounds_rect.bottom or pygame.sprite.spritecollideany(self, walls)

        #caso colida, retorna a posicao anterior
        if collide:
            self.image = oldImage
            self.rect = oldRect
            self.x = oldX
            self.y = oldY
        elif self.target: #se nao colide e o robot possui um target, atualiza a posicao do target
            self.target.x = self.x
            self.target.y = self.y
            self.target.rect.centerx = self.x
            self.target.rect.centery = self.y
            self.target.degree = self.degree
            self.target.speed = self.speed

        self.speed = 0

    def turnLeft(self):
        """Rotaciona a imagem para esquerda
        """
        self.degree += 1
        if self.degree >= 360:
            self.degree = 0

    def turnRight(self):
        """Rotaciona a imagem para direita
        """
        self.degree -= 1
        if self.degree < 0:
            self.degree = 355

    def turnUp(self):
        """ Velocidade positiva - anda para frente
        """
        self.speed = 1

    def turnDown(self):
        """ Velocidade negativa - anda para tras
        """
        self.speed = -1

    def calcPos(self):
        """ Calcula os novos x e y do robot, com base no angulo e na velocidade            
        """        
        self.dx = math.cos(math.radians(self.degree))
        self.dy = -math.sin(math.radians(self.degree))
        self.dx *= self.speed
        self.dy *= self.speed

    def dropTarget(self, arena):
        """ Larga o target, caso o robot esteja levando um
        """
        if self.target:
            #se estamos na arena
            if pygame.sprite.spritecollideany(self.target, arena):
                if self.target.x > 290:
                    incx = -40
                else:
                    incx = 40
                if self.target.y > 160:
                    incy = -40
                else:
                    incy = 40
            else:
                if ((self.target.x > 0 and self.target.x < 100 - 40) or (self.target.x > 700 and self.target.x < 800 - 40)) and (self.target.y > 100 - 40 and self.target.y < 440):                    
                    incx = 0
                elif self.target.x > 400:
                    incx = -40
                else:
                    incx = 40                    
                
                if (self.target.y > 0 and self.target.y < 100) and (self.target.x > 100 - 40 and self.target.x < 700):
                    incy = 0
                elif self.target.y > 540:
                    incy = -40
                else:
                    incy = 40

            self.target.x += incx
            self.target.y += incy
            self.target.rect.centerx += incx
            self.target.rect.centery += incy
            self.target = None
