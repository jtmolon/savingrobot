# -*- coding: utf-8 -*-

import pygame
import math

class Target(pygame.sprite.Sprite):
    def __init__(self, cor, x, y, size, screen):
        pygame.sprite.Sprite.__init__(self)    
        self.imageMaster = pygame.Surface(size)
        self.imageMaster.fill(cor)
        self.imageMaster.set_colorkey((255,1,0))
        self.imageMaster = self.imageMaster.convert()
        self.image = self.imageMaster
        self.rect = self.image.get_rect()
        self.rect.center = (x, y)
        self.x = self.rect.centerx
        self.y = self.rect.centery
        #angulo
        self.degree = 90
        #x/y temporario
        self.dx = 0
        self.dy = 0 
        #velocidade de movimentacao
        self.speed = 0

    def update(self):
        self.rect.centerx = self.x
        self.rect.centery = self.y
        self.calcPos()
        self.x += self.dx
        self.y += self.dy
        self.rect.centerx = self.x
        self.rect.centery = self.y
        self.speed = 0

    def set_x_y(self, x, y):
        self.x = x
        self.y = y

    def calcPos(self):
        """Calcula os novos x e y, conforme o angulo e a velocidade 
        """        
        self.dx = math.cos(math.radians(self.degree))
        self.dy = -math.sin(math.radians(self.degree))
        self.dx *= self.speed
        self.dy *= self.speed

