# -*- coding: utf-8 -*-
import math

from direct.showbase.ShowBase import ShowBase
from direct.task import Task
from direct.actor.Actor import Actor
from direct.interval.IntervalGlobal import Sequence
from panda3d.core import Point3, Texture, TextureStage
from pandac.PandaModules import *
from direct.gui.OnscreenText import OnscreenText

class SavingRobot(ShowBase):
    
    def __init__(self):
        ShowBase.__init__(self)

        # Disable the camera trackball controls.
        #self.disableMouse()

        #coloca um texto na tela
        self.title = OnscreenText(text="Saving Robot 3D",
                                  style=1, fg=(0,0,0,1),
                                  pos=(0.8,-0.95), scale = .07)
        #seta cor de fundo
        self.setBackgroundColor(0.3,0.0,0.0)

        # Cria o Traverser e handlers: primeiro passo para detectar colisao
        self.cTrav = CollisionTraverser()
        self.cTrav.setRespectPrevTransform(True)
        self.pusher = CollisionHandlerEvent()
        self.pusher.addInPattern('%fn-into-%in')
        self.pusher.addAgainPattern('%fn-again-%in')

        #Carrega o modelo e textura do chao
        self.chao = self.loader.loadModel("models/chao2")
        self.chao.reparentTo(self.render)
        self.chao.setPos(0,50,0)
        self.chao.setScale(2)
        textureChao = self.loader.loadTexture("textures/brick11.jpg")
        textureChao.setWrapU(Texture.WMRepeat)
        textureChao.setWrapV(Texture.WMRepeat)
        stageChao = TextureStage("Chao")
        stageChao.setSort(1)
        self.chao.setTexture(stageChao, textureChao)

        self.chaoCBox1 = CollisionNode("chaoCBox1")
        self.chaoCBox1.addSolid(CollisionBox(Point3(-30, 30, -1), Point3(30, 29, 5)))
        self.chaoBoxNP1 = self.chao.attachNewNode(self.chaoCBox1)
        self.chaoBoxNP1.show()

        self.chaoCBox2 = CollisionNode("chaoCBox2")
        self.chaoCBox2.addSolid(CollisionBox(Point3(-30, -29, -1), Point3(30, -30, 5)))
        self.chaoBoxNP2 = self.chao.attachNewNode(self.chaoCBox2)
        self.chaoBoxNP2.show()

        self.chaoCBox3 = CollisionNode("chaoCBox3")
        self.chaoCBox3.addSolid(CollisionBox(Point3(-29, 30, -1), Point3(-30, -30, 5)))
        self.chaoBoxNP3 = self.chao.attachNewNode(self.chaoCBox3)
        self.chaoBoxNP3.show()

        self.chaoCBox4 = CollisionNode("chaoCBox4")
        self.chaoCBox4.addSolid(CollisionBox(Point3(30, 30, -1), Point3(29, -30, 5)))
        self.chaoBoxNP4 = self.chao.attachNewNode(self.chaoCBox4)
        self.chaoBoxNP4.show()

        # Load the environment model
        self.arena = self.loader.loadModel("models/nova_arena")
        # Reparent the model to render
        self.arena.reparentTo(self.render)
        # Apply scale and position transforms on the model
        self.arena.setScale(2, 3, 2)
        self.arena.setPos(0, 85, 3)
        textureArena = self.loader.loadTexture("textures/tijolos.jpg")
        textureArena.setWrapU(Texture.WMRepeat)
        textureArena.setWrapV(Texture.WMRepeat)
        stageArena = TextureStage("Arena")
        stageArena.setSort(2)
        self.arena.setTexture(stageArena, textureArena)

        # Cria colliders: segundo passo
        self.arenaCBox1 = CollisionNode("arenaCBox1")
        self.arenaCBox1.addSolid(CollisionBox(Point3(-20, -1, -1), Point3(20, 2, 5)))
        self.arenaBoxNP1 = self.arena.attachNewNode(self.arenaCBox1)
        self.arenaBoxNP1.show()
        
        self.arenaCBox2 = CollisionNode("arenaCBox2")
        self.arenaCBox2.addSolid(CollisionBox(Point3(-18, 0, 0), Point3(-22, -23, 5)))
        self.arenaBoxNP2 = self.arena.attachNewNode(self.arenaCBox2)
        self.arenaBoxNP2.show()
        
        self.arenaCBox3 = CollisionNode("arenaCBox3")
        self.arenaCBox3.addSolid(CollisionBox(Point3(18, 0, 0), Point3(22, -23, 5)))
        self.arenaBoxNP3 = self.arena.attachNewNode(self.arenaCBox3)
        self.arenaBoxNP3.show()
        
        self.arenaCBox4 = CollisionNode("arenaCBox4")
        self.arenaCBox4.addSolid(CollisionBox(Point3(-20, -21, 0), Point3(-7, -24, 5)))
        self.arenaBoxNP4 = self.arena.attachNewNode(self.arenaCBox4)
        self.arenaBoxNP4.show()
        
        self.arenaCBox5 = CollisionNode("arenaCBox5")
        self.arenaCBox5.addSolid(CollisionBox(Point3(7, -21, 0), Point3(20, -24, 5)))
        self.arenaBoxNP5 = self.arena.attachNewNode(self.arenaCBox5)
        self.arenaBoxNP5.show()

        self.robot = self.loader.loadModel("models/robo_cubo2")
        self.robot.reparentTo(self.chao)
        self.robot.setScale(2, 2, 2)
        self.robot.setPos(0, -25, 3)
        self.posicao = self.robot.getPos()
        self.angulo = self.robot.getHpr()
        textureRobot = self.loader.loadTexture("textures/robo.jpg")
        textureRobot.setWrapU(Texture.WMRepeat)
        textureRobot.setWrapV(Texture.WMRepeat)
        stageRobot = TextureStage("Robo")
        stageRobot.setSort(3)
        self.robot.setTexture(stageRobot, textureRobot)

        # Cria colliders: segundo passo
        self.robotCEsfera = CollisionNode("robotCEsfera")
        self.robotCEsfera.addSolid( CollisionSphere(0,0,0, 1) )
        self.robotEsfNP = self.robot.attachNewNode(self.robotCEsfera)
        self.robotEsfNP.show()

        self.target = self.loader.loadModel("models/target")
        self.target.reparentTo(self.chao)
        self.target.setPos(0, 0, 2)
        textureTarget = self.loader.loadTexture("textures/warning.jpg")
        textureTarget.setWrapU(Texture.WMRepeat)
        textureTarget.setWrapV(Texture.WMRepeat)
        stageTarget = TextureStage("Target")
        stageTarget.setSort(4)
        self.target.setTexture(stageTarget, textureTarget)

        # Cria colliders: segundo passo
        self.targetCEsfera = CollisionNode("targetCEsfera")
        self.targetCEsfera.addSolid( CollisionBox(-1, 1) )
        self.targetEsfNP = self.target.attachNewNode(self.targetCEsfera)
        self.targetEsfNP.show()

        # Apontar ao Traverser e ao Handler, quais objetos devem ser observados ao colidir, e como tratar as colisões entre eles : terceiro passo
        self.cTrav.addCollider(self.robotEsfNP, base.pusher)
        self.cTrav.addCollider(self.arenaBoxNP1, base.pusher)
        self.cTrav.addCollider(self.arenaBoxNP2, base.pusher)
        self.cTrav.addCollider(self.arenaBoxNP3, base.pusher)
        self.cTrav.addCollider(self.arenaBoxNP4, base.pusher)
        self.cTrav.addCollider(self.arenaBoxNP5, base.pusher)
        self.cTrav.addCollider(self.chaoBoxNP1, base.pusher)
        self.cTrav.addCollider(self.chaoBoxNP2, base.pusher)
        self.cTrav.addCollider(self.chaoBoxNP3, base.pusher)
        self.cTrav.addCollider(self.chaoBoxNP4, base.pusher)

        #Array das teclas
        self.teclas = {"frente" : 0, "tras" : 0, "direita" : 0, "esquerda" : 0}
        #Seta teclas a serem usadas
        self.accept("arrow_up", self.setKey, ["frente",1])
        self.accept("arrow_up-up", self.setKey, ["frente",0])
        self.accept("arrow_down", self.setKey, ["tras",1])
        self.accept("arrow_down-up", self.setKey, ["tras",0])
        self.accept("arrow_right", self.setKey, ["direita",1])
        self.accept("arrow_right-up", self.setKey, ["direita",0])
        self.accept("arrow_left", self.setKey, ["esquerda",1])
        self.accept("arrow_left-up", self.setKey, ["esquerda",0])
        self.accept("space", self.dropTarget)
        self.accept("enter", self.positionCamera)
        #Adiciona uma função a lista de tasks
        self.gameTask = taskMgr.add(self.moveRobot, "moveRobot")
        self.gameTask.last = 0
        
        # Add the spinCameraTask procedure to the task manager
        #self.taskMgr.add(self.spinCameraTask, "SpinCameraTask")

        self.accept('robotCEsfera-into-targetCEsfera', self.getTarget)
        self.accept('robotCEsfera-again-targetCEsfera', self.getTarget)
        self.accept('robotCEsfera-into-arenaCBox1', self.moveNegative)
        self.accept('robotCEsfera-again-arenaCBox1', self.moveNegative)
        self.accept('robotCEsfera-into-arenaCBox2', self.moveNegative)
        self.accept('robotCEsfera-again-arenaCBox2', self.moveNegative)
        self.accept('robotCEsfera-into-arenaCBox3', self.moveNegative)
        self.accept('robotCEsfera-again-arenaCBox3', self.moveNegative)
        self.accept('robotCEsfera-into-arenaCBox4', self.moveNegative)
        self.accept('robotCEsfera-again-arenaCBox4', self.moveNegative)
        self.accept('robotCEsfera-into-arenaCBox5', self.moveNegative)
        self.accept('robotCEsfera-again-arenaCBox5', self.moveNegative)
        self.accept('robotCEsfera-into-chaoCBox1', self.moveNegative)
        self.accept('robotCEsfera-again-chaoCBox1', self.moveNegative)
        self.accept('robotCEsfera-into-chaoCBox2', self.moveNegative)
        self.accept('robotCEsfera-again-chaoCBox2', self.moveNegative)
        self.accept('robotCEsfera-into-chaoCBox3', self.moveNegative)
        self.accept('robotCEsfera-again-chaoCBox3', self.moveNegative)
        self.accept('robotCEsfera-into-chaoCBox4', self.moveNegative)
        self.accept('robotCEsfera-again-chaoCBox4', self.moveNegative)

    def getTarget(self, entrada):
        if self.target.getParent().getName() == "chao2.egg":
            self.target.reparentTo(self.robot)
            self.target.setScale(0.5)
            self.target.setPos(0, 0, 1.5)
 
    def dropTarget(self):
        if self.target.getParent().getName() == "robo_cubo2.egg":
            self.target.reparentTo(self.chao)
            self.target.setScale(1, 1, 1)
            incX = 0
            if not (self.robot.getX() < -24 or self.robot.getX() > 24):
                incX = 4
                if self.robot.getX() > 0:
                    incX = -4
            incY = 0
            if not (self.robot.getY() < -21 or self.robot.getY() > 23):
                incY = 4
                if self.robot.getY() > 0:
                    incY = -4
            if not (incX or incY):
                if (self.robot.getX() < -24 and self.robot.getY() < -21) or (self.robot.getX() < -24 and self.robot.getY() > 23):
                    incX = 4
                elif (self.robot.getX() > 24 and self.robot.getY() < -21) or (self.robot.getX() > 24 and self.robot.getY() > 23):
                    incX = -4

            self.target.setPos(self.robot.getX() + incX, self.robot.getY() + incY, 2)

    #Seta o estado das teclas
    def setKey(self, key, val):
        self.teclas[key] = val

    #Função responsavel por movimentar o robo    
    def moveRobot(self,task):
        deltaTime = task.time - task.last
        task.last = task.time
        speed = 0
        #Seta a nova posição do objeto
        if self.teclas["frente"]:
            speed = 0.2
        if self.teclas["tras"]:
            speed = -0.2
        if self.teclas["direita"]:
            self.angulo += Vec3(-1, 0, 0)
        if self.teclas["esquerda"]:
            self.angulo += Vec3(1, 0, 0)
        degree = self.angulo.getX()
        dy = math.cos(math.radians(degree)) * speed
        dx = -math.sin(math.radians(degree)) * speed
        self.posicao += Vec3(dx, dy, 0)

        self.robot.setPos(self.posicao)
        self.robot.setHpr(self.angulo)
        return Task.cont

    def moveNegative(self,entrada):
        """Move o robo ao contrario em caso de colisao
        """
        speed = 0
        if self.teclas["frente"]:
            speed = -0.2
        if self.teclas["tras"]:
            speed = 0.2
        if self.teclas["direita"]:
            self.angulo += Vec3(1, 0, 0)
        if self.teclas["esquerda"]:
            self.angulo += Vec3(-1, 0, 0)
        degree = self.angulo.getX()
        dy = math.cos(math.radians(degree)) * speed
        dx = -math.sin(math.radians(degree)) * speed
        self.posicao += Vec3(dx, dy, 0)

        self.robot.setPos(self.posicao)
        self.robot.setHpr(self.angulo)
        #return Task.cont
        print entrada

    def printSeTocando(self, entrada):
        print "Estao se tocando"
        print entrada
    
    # Define a procedure to move the camera
    def spinCameraTask(self, task):
        self.camera.setPos(0,-150,130)
        self.camera.setHpr(0, -35, 0)
        return Task.cont

    def positionCamera(self):
        self.camera.setPos(0,-60,60)
        self.camera.setHpr(0, -25, 0)

app = SavingRobot()
app.run()
