# -*- coding: utf-8 -*-

import pygame

class Wall(pygame.sprite.Sprite):
    
    def __init__(self, cor, (x, y, width, height)):
        pygame.sprite.Sprite.__init__(self)
        self.cor = cor
        self.image = pygame.Surface((width,height))
        self.image.fill((255,255,255))
        self.image.set_colorkey((255, 0, 0))
        pygame.draw.rect(self.image, self.cor, (0,0,width,height))
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
